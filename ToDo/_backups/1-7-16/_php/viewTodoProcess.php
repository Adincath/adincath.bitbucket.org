<?php

//Grabbing the title
if (isset($_GET["todoTitle"])) {
    $todoTitle = $_GET["todoTitle"];
    echo "<h1>" . $todoTitle . "</h1>";
} else {
    echo "<p>No data for the title </p> ";
}

// Grabbing the date
if (isset($_GET["todoDate"])) {
    $todoDate = $_GET["todoDate"];
    echo "<p>" . $todoDate . "</p>";
} else {
    echo "No data from Date" . "<br>";
}

//Grabbing all availible objectives
if (isset($_GET["todoObj"])) {
    $todoObj = $_GET["todoObj"];
    $objTotal = count($todoObj);
    echo "There are $objTotal objectives<br>";

    for ($index = 0; $index < $objTotal; $index++){
    	echo "<p>" . $todoObj[$index] . "</p>";
    }
} else {
    echo "No data from Obj" . "<br>";
}

// Creating the json object to be outputed
$userJson = "{\n\"title\": \"" . $todoTitle . "\",\n
        	   \"date\": \"" . $todoDate . "\",\n";

        	    // Creating the objectives section of the object except for the last 
        	    for ($index = 0; $index < $objTotal-1; $index++){
        	    	$userJson .= "\"objective_" . $index . "\":\"" . $todoObj[$index] . "\",\n";
				}

				// Last line of the json without the comma for valid JSON
        	    $userJson .= "\"objective_" . $index . "\":\"" . $todoObj[$index] . "\"\n";
        	   $userJson .= "\n}";

//echo "JSON: <br>" . $userJson;

// creating file name
$newJsonfile = $todoTitle . ".json";

// Outputting the JSON object to file with file name from $newJsonfile
file_put_contents("../_json/"$newJsonfile, $userJson);

//$test = str_replace("[", $userJson, $todoFile);

//file_put_contents($file, $test);

//Adding reference to-do to the list of to-do's
$todoListfile = "todoList.json";

// putting contents of file into an array
$todoListjson = file($todoListfile);

//Checking to see if the todoListfile has been initiliazed
echo $todoListjson[1] . "<br>";
if(rtrim($todoListjson[1]) == ']'){
	// excluding the comma after the last bracket
	$todoListjson[0] = "[\n{\"title\": \"" . $todoTitle . "\"}\n";
}
else{
	// including the comma after the last bracket
	$todoListjson[0] = "[\n{\"title\": \"" . $todoTitle . "\"},\n";
}

file_put_contents($todoListfile, $todoListjson);

?>