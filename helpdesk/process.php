<!DOCTYPE html>

<html>
    <head>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>HelpDesk Utilities</title>
        <link rel="stylesheet" type="text/css" href="../sa/_css/bootstrap-3.3.5/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="_css/main.css" />
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,greek,greek-ext' rel='stylesheet' type='text/css'>        <script src="../_js/jquery-1.7.2.js"></script>
		
		<script>
		
		$(document).ready(function(){
		   // cache the window object
		   $window = $(window);
		 
		   $('section[data-type="background"]').each(function(){
			 // declare the variable to affect the defined data-type
			 var $scroll = $(this);
							 
			  $(window).scroll(function() {
				// HTML5 proves useful for helping with creating JS functions!
				// also, negative value because we're scrolling upwards                             
				var yPos = -($window.scrollTop() / $scroll.data('speed')); 
				 
				// background position
				var coords = '50% '+ yPos + 'px';
		 
				// move the background
				$scroll.css({ backgroundPosition: coords });    
			  }); // end window scroll
		   });  // end section function
		}); // close out script
		
		</script>
		
    </head>
    <body>
    	<div class="container">
			<div>
				<ul class="nav nav-pills navbar-fixed-top">
					<li id="nav-overide" role="presentation" class="active"><a href="index.html">Home</a></li>
					<li id="nav-overide" role="presentation"><a href="email_scrubber.html">Email Scrubber</a></li>
					<li id="nav-overide" role="presentation"><a href="#">About</a></li>
				</ul>
			</div>
			
			

			
			<div id="strip" class="col-md-12">
				<img class="center image-rounded" src="_imgs/email_scrubber.jpg" alt="Email scrubber banner image">
			</div>
			
			<div class="col-md-12">
				<h1>Email Contents</h1>
				<?php
					$input = $_POST["input"];
					if ($input == ""){
						print "Something went wrong. Did you paste the contents?";
					}
					else{
						print  $input;
					}
				?>			
			</div>

        </div>
     
    </body>
</html>

