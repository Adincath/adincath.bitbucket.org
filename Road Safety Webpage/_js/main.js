			
			// Disabling caching, which was messing up the statistics of the questions. 
			jQuery.ajaxSetup({ cache: false });

			// index that will be used to reference image's in imageArray[] & Question in questionArray[], which are parallel arrays. 
			var index = 0; 
			
			// var that holds the total number of answers answered correctly
			var total_correct = 0; 
	
			// bool used to tell whether the sources tab is in the up/down position
			var sources_displyed = false;
			
			var primed = false;
			
			var updated = false;
			
			// array that holds the Questions. Length:13
			var questionArray = [
                              "1. You must be 16 to drive farm equipment", //False
							  "2. It is recommended that all farm equipment have a slow moving vehicle sign adhered to the back when travelling on public roads", //True
							  "3. The image on the screen is a Slow Moving Vehicle Sign", //True
							  "4. The image on the screen is a Speed Identification Sign", //True
							  "5. The image on the screen is a warning about the possibility of farm equipment on the road",//True
							  "6. Reflective symbols should be visible at a minimum of 25 feet  away to a driver behind equipment on the roadways at night",//True
							  "7. All agricultural equipment must have pilot cars when driving on public roadways",//False
							  "8. Tractors may have flashing lights of any color",//False
							  "9. Equipment wider than 12 feet must have reflective markers to show how wide it is",//False
							  "10. All Tractors with operator enclosures (cabs) shall have at least one rear-view mirror to permit the operator to see the highway behind the machine",//False
							  "11. It is legal to pass a Slow Moving Vehicle on a public road",//True
							  "12. Vehicle with slow-moving signs attached travel less than 20 mph",//True
							  "13. Farm equipment that is driven on public roads cannot legally take up more than one lane"//False
							];
			var answerKey = [	
								/*1*/"false",
								/*2*/"true",
								/*3*/"true",
								/*4*/"true",
								/*5*/"true",
								/*6*/"true",
								/*7*/"false",
								/*8*/"false",
								/*9*/"false",
								/*10*/"false",
								/*11*/"true",
								/*12*/"true",
								/*13*/"false"
								
							];
			
			// this array will hold all wrong answers
			var wrongAnswers = [];
			// this array will hold all of the correct answers. to used with updateJson()
			var correctAnswers = [];

						
			function animate_sources(){
				if($('#footer').hasClass('slide-up')) {
					$('#footer').addClass('slide-down', 700, 'easeOutBounce');
					$('#footer').removeClass('slide-up'); 
				} 
				else {
					$('#footer').removeClass('slide-down');
					$('#footer').addClass('slide-up', 700, 'easeOutBounce'); 
				}
			}
			
			function change_text(){
				// moving to the next step Question
				if (index  < 13){
	
					// changing the text on the screen to the next question held in questionArray
					document.getElementById("question_text").innerHTML =  questionArray[index] ;
					// priming the quiz: changing background, font-size, making buttons visible

					// if the index is matching up a question that requires an image in questionArray it displays the corresponding image.
					if (index == 2){
						document.getElementById("question_image").innerHTML = "<img src='_imgs/smv_sign.jpg'>";
					}
					else 
					if (index == 3){
						// image pulled from this site http://triadcatalog.com/store/images/45mph.jpg
						document.getElementById("question_image").innerHTML = "<img src='_imgs/speedIDsign.jpg'>";							
					}
					else 
					if (index == 4){
						// image pulled from this site http://www.streetsignusa.com/images/w11-5ra16.gif
						document.getElementById("question_image").innerHTML = "<img src='_imgs/farmingequipsign.gif'>";					
					}
					
					// else if the index is looking at a question that does not require an image
					else {
						document.getElementById("question_image").innerHTML =  " ";
					}	
										
					// adding width to the progress bar 
					increment_progress_bar(index);
				}
				
				
				// At the end of the quiz
				else{
					
					// makes the results message and adds it to the end of the questionArray
					display_results();
					
				}	
				
				
			}
			
			function check_answer(index, answer){

				if(index <= 13){		
				
					if (answer == answerKey[index]){
					
						total_correct++;
						correctAnswers.push(index);
						
					}
					else{
						
						wrongAnswers.push(index);
						
					}
				
				} 
			
			}
			
			function display_results(){
				
				// collecting stats
				updateStats();				
				// var that will come at the end of the test to show results and is stored as the last element in the questionArray
				var results; 
				var width = $(window).width();
				document.getElementById("question").style.paddingTop = "5vh";		
				document.getElementById("true_toggle").innerHTML = "<p id='try_again'>Try Again?</p>";		
				document.getElementById("false_toggle").innerHTML = "<p id='view_results'>Finish Quiz</p>";	
				/* Basically making sure that the change in buttons stays responsive. 
				   Unfortunately this is based on the screen width at the time that the function is called. 
				   So if display_results() is called and then the screen changes size the buttons won't look good */
				if(width > 1024){
					// changing the style for the last part of the quiz
					$('#true_toggle').css('height', "90px") ;
					$('#false_toggle').css('height', "90px") ;		
				}
				else
				if(width <= 1024 && width > 700){
					// changing the style for the last part of the quiz
					document.getElementById("true_toggle").style.height = "90px";		
					document.getElementById("false_toggle").style.height = "90px";	

				}
				if(width <= 700 && width > 480){
					// changing the style for the last part of the quiz
					document.getElementById("true_toggle").style.height = "50px";	
					document.getElementById("false_toggle").style.height = "50px";	
					document.getElementById("true_toggle").style.width = "100px";		
					document.getElementById("false_toggle").style.width = "100px";
					document.getElementById("try_again").style.paddingTop = "12px";		
					
					document.getElementById("sources").style.height = "275px"; 		
				}
				if(width <= 480){
					// changing the style for the last part of the quiz
					document.getElementById("true_toggle").style.height = "40px";		
					document.getElementById("false_toggle").style.height = "40px";	
					document.getElementById("true_toggle").style.width = "100px";		
					document.getElementById("false_toggle").style.width = "100px";
					document.getElementById("try_again").style.paddingTop = "5px";	
					document.getElementById("try_again").style.paddingBottom = "80px";		
					document.getElementById("view_results").style.paddingTop = "5px";						

				}				
		
				// Changing the functions called by onclick back to check answers again
				document.getElementById('true_toggle').onclick = function () { restart() }; 	
				document.getElementById('false_toggle').onclick = function () { view_answers() }; 
				
				results = "You answered " + total_correct + " out of 13 correct";
				document.getElementById("question_text").innerHTML =  results;
			}
				
			function fadeIn(){
				// packaging all of the fadeIn()'s that need happen into one function			
				$( "#question_text" ).fadeIn( "slow");
				$( "#true_toggle" ).fadeIn( "slow");
				$( "#false_toggle" ).fadeIn( "slow");
				$( "#question_image" ).fadeIn( "slow");
			}
				
			function fadeOut(){
				
				// packaging all of the fadeOut()'s that need happen into one function				
				$( "#question_text" ).fadeOut( "slow", function() {  
					// will change the question text once old question has faded out completely
					change_text();	
					
				});
				
				$( "#true_toggle" ).fadeOut( "slow" );
				$( "#false_toggle" ).fadeOut( "slow" );
				$( "#question_image" ).fadeOut( "slow" );
					
			}
									
			function forward(answer){
				check_answer(index, answer);
				index++;
				fadeOut();
				fadeIn();
			}
			
			function increment_progress_bar(progress){
				var pBar = document.getElementById('progress_bar');
						
				if (progress <= 13){
				  pBar.value = progress + 1;
				}
			}
			
			function prime(){	
						
				if (!primed){
					$( "#question_text" ).fadeOut( 'slow', function(){
						$('#quiz_text_container #question').css('font-size', "36px" ) ;
						$('#quiz_text_container #question_text').css('cursor', "text" ) ;
						change_text();
								
						$( "#question_text" ).fadeIn( "slow");
						$( "#true_toggle" ).fadeIn( "slow");
						$( "#false_toggle" ).fadeIn( "slow");
					});
					$( "#question_image" ).fadeOut( 'slow' );		
					primed = true;		
				}
				
			}
				
			function restart(){
				$( "#false_toggle" ).fadeOut( "slow" );
				$( "#question_text" ).fadeOut( "slow" );
				$( "#true_toggle" ).fadeOut( "slow", function(){
						// refreshing the page
						window.location.reload()
				});

			}
			
			/*
				This is the only js function that is using php. If someone edits the _php/stats.php at all and saves it, that files permissions will be set back to the default. 
				This means that that files won't have permission to edit other files on the server.
				To fix this, someone needs to go onto a windows machine, map to where the web pages are being held, right click on the stats.php file, go to security, find the apache user, and allow it total access. 
				Then do the same for the _js/stats.json file I think
				
				This function also depends about the _js/stats.json file to already have a valid json object in it already. There is a default file in there called, "stat-default.json" that already has the object with all zero'd out stats. 
				Because the php overwrites the old json object, if something was done incorrectly, it can overwrite the json object to be blank. 
				If there is no json object in the file or if it is just blank, the browser's console will give an error like, "jQuery can't find the length of NUll" because there is no object for it to find length of. 
			*/
			function updateStats()
			{
				if(updated == false){
										
					// String that will be edited to show the most current stats for the questions
					var questionStats = "{";
					
					// Opening the json file that holds the stats that arent updated yet
					$.getJSON("_js/stats.json" , function( data ) {
						
						// parsing the json file
						$.each(data, function(key, val){
							//seeing if a question stat needs to be updated
							for(var index = 0; index <= 12; index++){
								if(correctAnswers[index] == key){
									val++;
								}
							}
							// Adding to one to total so we can see how many people got it right or wrong
							if("total" == key){
								val += 1;
							}
							
							// concatinating json items 
							questionStats = questionStats.concat("\"" , key ,"\"", ": ", val, ",");
						});
						
						// ninja'ing that last comma out so that the string is valid json
						questionStats = questionStats.slice(0, questionStats.length - 1);
						// adding closing bracket to the json file
						questionStats = questionStats.concat("}");
						
						console.log(questionStats);
						
						// Sending string to stats.php so that php can write the string to the file "stats.json" 
						$.post( "_php/stats.php" ,{questionStats: questionStats});
					});	
				}
				updated = true;
			}

			
			function view_answers(){
				console.log("view_answers()")
				if(index <= 13){
					
					index++;
					
					// This var will hold the HTML text that will be displayed 
					var resultText = "";
					
					$( "#question_text" ).fadeOut( "slow", function() { 
						
						// Loading resultText with the wrongAnswers
						for (var index = 0; index < wrongAnswers.length;index++){
							
							resultText += "<p>" + questionArray[wrongAnswers[index]] + " is <b>" + 
							answerKey[wrongAnswers[index]] + "</b>.</p>";
							
						}
						
						$('#quiz_text_container #question').css('font-size', "24px" ) ;
						$('#quiz_text_container #question_text').css('padding', "5px" ) ;
						document.getElementById("question_text").innerHTML = "<p> Thank you for taking our quiz! <br> <img src='_imgs/Soybean_board.jpg' style='box-shadow: 0px 9px 20px; margin-top: 40px;'> <br><h3>Wrong Answers:</h3> <br> " + resultText + "&nbsp;</p>";
						
					});
					
					$( "#true_toggle" ).fadeOut( "slow", function(){
					
						$('#true_toggle').css('margin-left', "auto" ) ;
						$('#true_toggle').css('margin-right', "auto" ) ;
						$('#true_toggle').css('float', "none" ) ;
						
					});
					
					$( "#false_toggle" ).fadeOut( "slow");
					
					$( "#question_image" ).fadeOut( "slow", function(){ 
					
						document.getElementById("question_image").innerHTML = "";

					});
					
					$( "#question_text" ).fadeIn( "slow");
					$( "#true_toggle" ).fadeIn( "slow");
					//fadeIn();
				}

			}
			
