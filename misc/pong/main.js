var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var ballRadius = 10;
var x = canvas.width/2;
var y = canvas.height-30;
var dx = 2;
var dy = -2;
var paddleHeight = 75;
var paddleWidth = 10;
var dividerWidth = 5;
var dividerHeight = canvas.height;
var paddleY1 = (canvas.height -paddleHeight)/2;
var paddleY2 = (canvas.height -paddleHeight)/2;
var upPressed1 = false;
var downPressed1 = false;
var upPressed2 = false;
var downPressed2 = false;
var p1Score = 0;
var p2Score = 0;

document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

function keyDownHandler(e) {
    if(e.keyCode == 40) {
        upPressed1 = true;
    }
    else if(e.keyCode == 38) {
        downPressed1 = true;
    }
    if(e.keyCode == 83) {
        upPressed2 = true;
    }
    else if(e.keyCode == 87) {
        downPressed2 = true;
    }
}
function keyUpHandler(e) {
    if(e.keyCode == 40) {
        upPressed1 = false;
    }
    else if(e.keyCode == 38) {
        downPressed1 = false;
    }
    if(e.keyCode == 83) {
        upPressed2 = false;
    }
    else if(e.keyCode == 87) {
        downPressed2 = false;
    }    
}
function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI*2);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}
function drawPaddle1() {
    ctx.beginPath();
    ctx.rect(canvas.width-paddleWidth, paddleY1, paddleWidth, paddleHeight);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}
function drawPaddle2() {
    ctx.beginPath();
    ctx.rect(0, paddleY2, paddleWidth, paddleHeight);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}
function drawDivider(){
    ctx.beginPath();
    ctx.rect(canvas.width/2, 0, dividerWidth, dividerHeight )
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}
function drawScore(){
    ctx.font = "30px Arial";
    ctx.fillText(p1Score + " | " + p2Score, canvas.width/2 - 27, 30);
}
function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBall();
    drawPaddle1();
    drawPaddle2();
    drawDivider()    
    drawScore()    

    // If the ball hits a wall, it "bounce" and change directions
    if(x + dx < ballRadius) {
        dx = -dx;
    }
    else if(x + dx > canvas.width-ballRadius || x + dx == ballRadius) {
        if(y > paddleY1 && y+paddleWidth < paddleY1 + paddleHeight || y > paddleY2 && y+paddleWidth < paddleY2 + paddleHeight) {
            dx = -dx;
        }
        else {
            if (x+dx == ballRadius){
                p1Score++;
            }
            else{
                p2Score++;
            }
        x = canvas.width/2;
        y = canvas.height-30;
        }
    }

    if(y + dy > canvas.height-ballRadius || y + dy < ballRadius) {
        dy = -dy;
    }


    // Checking to see if the paddle 1 needs to be moved
    if(upPressed1 && paddleY1 < canvas.height-paddleHeight) {
        paddleY1 += 7;
    }
    else if(downPressed1 && paddleY1 > 0) {
        paddleY1 -= 7;
    }
   
    // Checking to see if the paddle 2 needs to be moved
    if(upPressed2 && paddleY2 < canvas.height-paddleHeight) {
        paddleY2 += 7;
    }
    else if(downPressed2 && paddleY2 > 0) {
        paddleY2 -= 7;
    }

    x += dx;
    y += dy;
}

setInterval(draw, 10);