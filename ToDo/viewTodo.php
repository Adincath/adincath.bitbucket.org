<!DOCTYPE html>
<html>

<head>
    <title>To Do List</title>
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
    <!-- Font used -->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <!-- main css -->
    <link rel="stylesheet" type="text/css" href="_css/main.css">

    <script>
/**
 * The Sign-In client object.
 */
var auth2;

/**
 * Initializes the Sign-In client.
 */
var initClient = function() {
    console.log("initClient");
    gapi.load('auth2', function(){
        /**
         * Retrieve the singleton for the GoogleAuth library and set up the
         * client.
         */
        auth2 = gapi.auth2.getAuthInstance({
            client_id: 'CLIENT_ID.apps.googleusercontent.com'
        });

        // Attach the click handler to the sign-in button
        auth2.attachClickHandler('signin-button', {}, onSuccess, onFailure);
    });
};

/**
 * Handle successful sign-ins.
 */
var onSuccess = function(user) {
    console.log('Signed in as ' + user.getBasicProfile().getName());
 };

/**
 * Handle sign-in failures.
 */
var onFailure = function(error) {
    console.log(error);
};

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  var email = profile.getEmail();
  initClient();
  document.getElementById("userEmail").value = email;  
}    </script>

</head>

<body>

  <?php 

    // Server Meta Information
    $servername = "localhost";
    $username = "root";
    $password = "TabletStrategyCan!";
    $dbname = "todo";
    $tablename = "todoTable";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // SQL Statement to be used
    $sql = "SELECT title FROM $tablename";

    // Creating array to hold titles
    $titles = array();

    //Querying the DB
    $result = mysqli_query($conn, $sql);

    //For every title availble it will store it in the array
    while(($row = mysqli_fetch_array($result))){
        $titles[] = $row['title'];  
    }

    //$titles = json_encode($titles);

    $conn->close();

  ?>

    <?php include "nav.html" ?>

    <div class="col-sm-4" id="main-container">
        <!-- Editing a ToDo form -->
        <h1> View a To-Do List </h1>
        <hr>
        <form action="editTodo.php" action="get">
            <p>ToDo Group</p>
            <select name="todoTitle">
            	<?php 
            	for($i = 0; $i < count($titles); $i +=1){
                	echo "<option value=\"$titles[$i]\"> $titles[$i] </option>";
            	}
                ?>
            </select>
            <br>
            <input class="btn btn-primary" type="submit" action="">
        </form>
    </div>

</body>

</html>