
<?php
	
updateStats($_POST["questionStats"]);

// This function takes in a string that contains a json object with the current question stats and updates the old json object at "../_js/stats.json"
function updateStats($questionStats){

    // The file that holds the old json stats
	$file = '../_js/stats.json';
	
	// Makes sure that it got an actual string from ../_js/main.js. If someone were to just load stats.php this function will fire with an empty string
	if (isset($questionStats)){
		// Open the file to put updated stats
		file_put_contents($file, $questionStats);
		backupStats($questionStats);
	}
	

}

//Takes the updated stats from updateStats() and writes them to backups.txt
function backupStats($questionStats){

    // The file that will be backed up to
	$file = 'backups.txt';
		
	// Open the file to get existing content
	$current = file_get_contents($file);

	// This is the string that will be prepended to the backup.txt. String is a date a : and then the stats
	$current = date("Y/m/d"). ": " . $questionStats . "\n" . $current;
	
	// Write the contents back to the file
	file_put_contents($file, $current);
	
	/*
	    This last section of code makes sure that the backup only keeps the last thousand stats. NOTE: Everytime someone takes the quiz the stats are backed up.
	*/
	
	// file() return an array of the file that is stored in $lines
	$lines = file($file);
		
	// If there are more than 1000 backups, this loop will pop the oldest stats out of the file 
	while(count($lines) > 1000){
		array_pop($lines);
	}
	
	// Writing the correctly sized array back to the file
	file_put_contents($file, $lines);
	
}

?>