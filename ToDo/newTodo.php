<!DOCTYPE html>
<html>

<head>
    <title>To Do List</title>
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
    <!-- Font used -->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <!-- main css -->
    <link rel="stylesheet" type="text/css" href="_css/main.css">
    <!-- Google Platform Library -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="812054113962-j7ev61u562k51v34k6cvvdg92ftl1btp.apps.googleusercontent.com"><script>
  
var objCount = 0;

function addObjective(){
  objCount+=1;
  var objHtml = [];

  var queue = [];

  // Loading queue with objectives
  for(var i= 0;i < objCount;i++){
    var id = 'obj' + i;
    var objective = document.getElementById(id).value;
    queue.push(objective);
  }

  for(var i= 0;i < objCount;i++){
    objHtml.push("<h3> " + (i+1) + ": </h3><input id=\"obj" + i + "\" type=\"text\" name=\"todoObj[]\" value=\"" + queue[i] + "\"><br>");
  }

  objHtml.push("<h3> " + (objCount+1) + ": </h3><input id=\"obj" + objCount + "\" type=\"text\" name=\"todoObj[]\" value=\"\"><br>");
 
  document.getElementById("objectives").innerHTML = objHtml.join('');
}

/**
 * The Sign-In client object.
 */
var auth2;

/**
 * Initializes the Sign-In client.
 */
var initClient = function() {
    console.log("initClient");
    gapi.load('auth2', function(){
        /**
         * Retrieve the singleton for the GoogleAuth library and set up the
         * client.
         */
        auth2 = gapi.auth2.getAuthInstance({
            client_id: 'CLIENT_ID.apps.googleusercontent.com'
        });

        // Attach the click handler to the sign-in button
        auth2.attachClickHandler('signin-button', {}, onSuccess, onFailure);
    });
};

/**
 * Handle successful sign-ins.
 */
var onSuccess = function(user) {
    console.log('Signed in as ' + user.getBasicProfile().getName());
 };

/**
 * Handle sign-in failures.
 */
var onFailure = function(error) {
    console.log(error);
};

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  var email = profile.getEmail();
  initClient();
  document.getElementById("userEmail").value = email;  
}

</script>    
</head>

<body>
    <?php include "nav.html" ?>

    <div class="col-sm-4" id="main-container">
        <!-- New Todo Form -->
        <h1> Create a new To-Do List </h1>
        <hr>
        <form action="http://localhost/broabect/adincath.bitbucket.org/ToDo/_php/newTodoProcess.php" method="get">
            <!--<form action="newTodoProcess.php" method="get">-->
            <h3>Email</h3>
            <input id="userEmail" type="text" name="userEmail" readonly>
            <h3>Title</h3>
            <input type="text" name="todoTitle">
            <h3>Description</h3>
            <textarea id="desc-textarea" name="todoDesc"></textarea>
            
            <h3>Due Date</h3>
            <input id="date" type="date" name="todoDate">

            <h3>Objectives</h3>
            <div id="objectives">

              <h3> 1: </h3><input id="obj0" type="text" name="todoObj[]"><br>

            </div>
              <button type="button" class="btn btn-success" onclick="addObjective()">Add Objective</button>
            <br>
            <br>
            <br>
            <input class="btn btn-primary" type="submit" onclick="addEmail">
        </form>
    </div>
</body>

</html>
