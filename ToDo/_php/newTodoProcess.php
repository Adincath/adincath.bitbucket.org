<?php
//Grabbing the title
if (isset($_GET["userEmail"])) {
    $userEmail = $_GET["userEmail"];
    echo "<h1>" . $userEmail . "</h1>";
} else {
    echo "<p>No data for the title </p> ";
}

//Grabbing the title
if (isset($_GET["todoTitle"])) {
    $todoTitle = $_GET["todoTitle"];
    echo "<h1>" . $todoTitle . "</h1>";
} else {
    echo "<p>No data for the title </p> ";
}

// Grabbing the desc
if (isset($_GET["todoDesc"])) {
    $todoDesc = $_GET["todoDesc"];
    echo "<p>" . $todoDesc . "</p>";
} else {
    echo "No data from Description" . "<br>";
}

// Grabbing the date
if (isset($_GET["todoDate"])) {
    $todoDate = $_GET["todoDate"];
    echo "<p>" . $todoDate . "</p>";
} else {
    echo "No data from Date" . "<br>";
}

//Grabbing all availible objectives
if (isset($_GET["todoObj"])) {
    $todoObj  = $_GET["todoObj"];
    $objTotal = count($todoObj);
    echo "There are $objTotal objective(s)<br>";
    
    for ($index = 0; $index < $objTotal; $index++) {
        echo "<p>" . $todoObj[$index] . "</p>";
    }
} else {
    echo "No data from Obj" . "<br>";
}

// Creating the json object to be outputed
$userJson = "{\n";

// Creating the objectives section of the object except for the last 
for ($index = 0; $index < $objTotal - 1; $index++) {
    $userJson .= "\"objective_" . $index . "\":\"" . $todoObj[$index] . "\",\n";
}

// Last line of the json without the comma for valid JSON
$userJson .= "\"objective_" . $index . "\":\"" . $todoObj[$index] . "\"\n";
$userJson .= "}";


include "dbMeta.php";

//Attempting connection and query
try{
    $conn = new PDO('mysql:dbname=' . DB_DATABASE . ';host=' . DB_HOST  . ';' ,DB_USERNAME , DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  

    $params = array($userEmail, $todoTitle, $todoDate, $todoDesc, $userJson);
    $sql = "INSERT INTO "  . DB_TABLE . " VALUES (NULL, :userEmail, :todoTitle, :todoDate, :todoDesc, :userJson)";

    $query = $conn->prepare($sql);

    //Adding Paremterized Variables
    $query->bindParam(':userEmail', $userEmail);
    $query->bindParam(':todoTitle', $todoTitle);
    $query->bindParam(':todoDate', $todoDate);
    $query->bindParam(':todoDesc', $todoDesc);
    $query->bindParam(':userJson', $userJson);

    $query->execute();

    $conn = NULL;      
} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}


?>