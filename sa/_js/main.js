	     <!-- Routing Controller --> 
	     var mainApp = angular.module("mainApp", ['ngRoute']);
			 mainApp.config(['$routeProvider', function($routeProvider) {
				 $routeProvider.
				 
				 when('/about', {
					 templateUrl: 'about.html',
					 title: 'About'
				 }).
				 
				 when('/admin', {
					 templateUrl: 'admin.html',
					 controller: 'adminController',
					 title: 'Admin'
				 }).
				 when('/contact', {
					 templateUrl: 'contact.html',
					 title: 'Contact Us'
				 }).				 
				 when('/events', {
					 templateUrl: 'events.html',
					 controller: 'eventsController',
					 title: 'Events'
				 }).
				 when('/faq', {
					 templateUrl: 'faq.html',
					 title: 'FAQ'
				 }).
			     when('/gallery', {
					 templateUrl: 'gallery.html',
					 title: 'Photo Gallery'
				 }).
				 when('/welcome', {
					 templateUrl: 'welcome.html',
					 controller: 'indexController',
					 title: 'Welcome'
				 }).
	
				 when('/members', {
					 templateUrl: 'members.html',
					 controller: 'membersController',
					 title: 'Members'
				 }).
	
				 when('/philanthropy', {
					 templateUrl: 'philanthropy.html',
					 controller: 'philanthropyController',
					 title: 'Philanthropy'
				 }).
	
	
	
				 otherwise({
					 redirectTo: '/welcome'
				 });
				 

	     }]);

			mainApp.run(['$location', '$rootScope', function($location, $rootScope) {
			    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {

			        if (current.hasOwnProperty('$$route')) {

			            $rootScope.title = current.$$route.title;
			        }
			    });
			}]);
	     mainApp.controller('adminController', function($scope, $http) {
	         $scope.json_file = "data/test.json";
	         $scope.message = "In adminController";
	         $scope.inUse = false;
	         $scope.using = function() {
	             return !inUse
	         };

	         $http.get($scope.json_file).
	         success(function(data, status, headers, config) {
	             $scope.page = data.pages;
	         }).
	         error(function(data, status, headers, config) {
	             console.log("Error opening JSON file");
	         });


	     });
         mainApp.controller('eventsController', function($scope) {});
	     mainApp.controller('indexController', function($scope, $http, $location, $anchorScroll) {


	         $scope.json_file = "data/test.json";
	         $http.get($scope.json_file).
	         success(function(data, status, headers, config) {
	             $scope.page = data.index_nav;
	         }).
	         error(function(data, status, headers, config) {
	             console.log("Error opening JSON file: " + $scope.json_file);
	         });
			 
			 $scope.gotoSlice = function() {
			     $location.hash('events');
				 
				 $anchorScroll();
		     };
			 
	     });

	     mainApp.controller('membersController', function($scope) {
	     	$scope.pageTitle = "Members";

	         $scope.EC = [{
	             name: 'Jenny',
	             title: 'President',
	             headshot: '../sa/_imgs/cat_headshot copy.jpg'
	         }, {
	             name: 'Kathy',
	             title: 'Vice President',
	             headshot: '../sa/_imgs/cat_headshot copy.jpg'
	         }, {
	             name: 'Hope',
	             title: 'Treasurer',
	             headshot: '../sa/_imgs/cat_headshot copy.jpg'
	         }, {
	             name: 'Hannah',
	             title: 'Public Relations',
	             headshot: '../sa/_imgs/cat_headshot copy.jpg'
	         }, {
	             name: 'Joe Bob',
	             title: 'Secretary',
	             headshot: '../sa/_imgs/cat_headshot copy.jpg'
	         }, {
	             name: 'Sally',
	             title: 'Historian',
	             headshot: '../sa/_imgs/cat_headshot copy.jpg'
	         }];

	     });
		 
		mainApp.controller('navController' , function($scope, $location) {
			$scope.isActive = function (viewLocation) { 
				  	return viewLocation === $location.path();
			};			

			/*
			* Simulates a click on the nav button to re collapse the mobile nav
			*/
			$scope.collapseNav= function(){
			    $(".navbar-collapse").collapse('hide');
          		//returning scroll to top of page
				$('html,body').scrollTop(0);
			};
     	});

	     mainApp.controller('philanthropyController', function($scope) {

	     });

