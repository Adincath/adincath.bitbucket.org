   <!-- Routing Controller --> 
   var mainApp = angular.module("mainApp", ['ngRoute', 'ui.bootstrap']);
   mainApp.config(['$routeProvider', function($routeProvider) {
       $routeProvider.

       when('/acm', {
           templateUrl: 'acm.html',
       }).
       when('/breakout', {
           templateUrl: 'projects/breakout.html',
       }).
       when('/helpdesk', {
           templateUrl: 'projects/help-desk-utilities.html',
       }).
       when('/index', {
           templateUrl: 'welcome.html',
           controller: 'indexController'
       }).       
       when('/memeGen', {
           templateUrl: 'projects/memeGen.html'
       }).
       when('/projects', {
           templateUrl: 'projects.html'
       }).
       when('/resources', {
           templateUrl: 'resources.html',
           controller: 'resourcesController'
       }).
       when('/road-quiz', {
           templateUrl: 'projects/soybean-road-quiz.html',
       }).
       when('/sa', {
           templateUrl: 'projects/sa-test-site.html',
       }).       
       when('/todo', {
           templateUrl: 'projects/ToDo-List.html',
       }).
       when('/ToDo', {
           templateUrl: 'projects/ToDo-List.html',
       }). 
       when('/the-directory', {
           templateUrl: 'projects/the-directory.html',
       }).                    
       when('/utilities', {
           templateUrl: 'utilities.html',
           controller: 'utilitiesController'
       }).
       otherwise({
           redirectTo: '/index'
       });
   }]);

   mainApp.controller('commentController', function($scope, $http) {
	     $scope.json_file = "_js/comments.json";
         $scope.commenting = false;
		 
         $scope.date = new Date();
         var dd = $scope.date.getDate();
         var mm = $scope.date.getMonth() + 1; //January is 0!
         var yyyy = $scope.date.getFullYear();


	   $scope.user = '';
	   $scope.comment = '';
	   $scope.date = mm + '/' + dd + '/' + yyyy;
	   
       $scope.persistComment = function() {	
           $.post("_php/persistComment.php", {
               user: $scope.user,
               comment: $scope.comment,
               date: $scope.date
           });
		   
		   $scope.user = null;
		   $scope.comment = null;
       };
	   $scope.cancelComment = function(){
		   $scope.user = null;
		   $scope.comment = null;		   
	   };

       $scope.loadComments = function() {

            $http.get($scope.json_file).
               success(function(data, status, headers, config) {
                   $scope.comments = data;
               }).
               error(function(data, status, headers, config) {
                   console.log("Error opening JSON file");
               });

        }
        // Initial load of comments
        $scope.loadComments();

   });

   mainApp.controller('indexController', function($scope) {
       $scope.breadcrumbs = [{
           "title": "utm",
           "url": "http://www.utm.edu/"
       }, {
           "active": "active",
           "title": "broabect",
           "url": "#index"
       }];
   });

   mainApp.controller('navController', function($scope, $location, $timeout) {
       $scope.flipped = true;
       /*
       * Takes the the current page and returns true if the navigation href
       * matches the URL
       */
       $scope.isActive = function(viewLocation) {
           return viewLocation === $location.path();
       };

       /*
       * Simulates a click on the nav button to re collapse the mobile nav
       */
       $scope.collapseNav= function(){
          $(".navbar-collapse").collapse('hide');
          //returning scroll to top of page
          $('html,body').scrollTop(0);
       };

       /*
       *  If the devices screen is less then 767px it will stack the navigations
       *  by removing the Bootstrap class '.nav-pills'
       */
       $scope.stackNav = function(){
         if(window.innerWidth >= 767){
           return true;
         }
         else{
          return false;
         }
       };

       $scope.toggleFlipped = function(flag){
              // Keeps the brand from flipping when the nav is closed
              if(flag == 'brand' && $scope.flipped == true){
                  $scope.flipped = true;
              }
              else if ($scope.flipped == true ){
                  $scope.flipped = false;
              }
              else{
                  $scope.flipped = true;
              }
       };

   });

   mainApp.controller('resourcesController', function($scope) {
       $scope.breadcrumbs = [

           {
               "title": "utm",
               "url": "http://www.utm.edu/"
           }, {
               "title": "broabect",
               "url": "#index"
           }, {
               "active": "active",
               "title": "resources",
               "url": "#resources"
           }

       ];
   });

   mainApp.controller('utilitiesController', function($scope) {
       $scope.breadcrumbs = [

           {
               "title": "utm",
               "url": "http://www.utm.edu/"
           }, {
               "title": "broabect",
               "url": "#index"
           }, {
               "active": "active",
               "title": "utilities",
               "url": "#utilities"
           }

       ];
       $scope.message = "I am the resources controller message!";
   });