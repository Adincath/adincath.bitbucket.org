<!DOCTYPE html>
<html>
    <head>
        <title>To Do List</title>
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <!-- Bootstrap -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
        <!-- Font used -->
        <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <!-- main css -->
        <link rel="stylesheet" type="text/css" href="_css/main.css">
        <script>
        var editingTodo = $('#editTodoToggle').prop('checked');

        var toggle = function(){
            editingTodo = $('#editTodoToggle').prop('checked');
            if(editingTodo){
                $("#EditForm").show();
                $("#unEditForm").hide();
            }
            else{
                $("#EditForm").hide();
                $("#unEditForm").show();
            }
        };

        var showDelete = function(){
            console.log("in showDelete");
            $("#EditForm").hide();
            $("#toggleLabel, #editTodoToggle").hide();
            $("#DeleteForm").show();
        };
        var deleteTodo = function(flag){
            if(flag === true){
                <?php deleteTodo() ?>
            }
            else{
                $("#EditForm").show();
                $("#toggleLabel, #editTodoToggle").show();
                $("#DeleteForm").hide();
            }
        };

        </script>
    </head>
    <body>
        <?php

            //Using this with the delete function if necassary
            session_start();


            //Grabbing the title
            if (isset($_GET["todoTitle"])) {
                $todoTitle = $_GET["todoTitle"];
            } else {
                echo "<p>No data for the title </pD> ";
            }
            $servername = "localhost";
            $username = "root";
            $password = "TabletStrategyCan!";
            $dbname = "todo";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
            }
            settype($todoTitle, "string");
            $sql = "SELECT * FROM todoTable WHERE title = '$todoTitle'";
            if($result = mysqli_query($conn, $sql)){
                    $row = mysqli_fetch_row($result);
            }

            $columnNames = array("Title", "dueDate", "Description", "Json");

            function deleteTodo() {
                
            }

        ?>
        <?php include "nav.html" ?>
        <div class="col-sm-4" id="main-container">
            <!-- echoing title of the to do -->
            <h1> <?php echo $row[0]  ?></h1>
            <hr>
            <span id="toggleLabel">Edit ToDo:</span><input id="editTodoToggle" onclick="toggle()" type="checkbox"> 

            <!-- Todo View --> 
            <div id="unEditForm">
            <?php 
                // less than 4 here b/c there should be 4 columns in the db table
                for ($i = 0; $i < count($columnNames); $i+=1){
                    echo "<p><strong>" . $columnNames[$i] . "</strong>: " . $row[$i] . "</p>";
                }
            ?>
            </div>

            <!-- Todo Edit -->
            <div  id="EditForm">
                <!--<form action="http://localhost/broabect/adincath.bitbucket.org/_php/editTodoProcess.php">-->
                <form action="http://localhost/broabect/ToDo/_php/editTodoProcess.php">
                <?php 
                    // for loop is based on the $columnName variable
                    for ($i = 0; $i < count($columnNames); $i+=1){
                        settype($row[$i], "string");
                        if($i != 3){
                            echo "<strong>" . $columnNames[$i] . "</strong>: <input name=\"" . $columnNames[$i] . "\" type=\"text\" value=\"" .  $row[$i] . "\"><br>";
                        }
                        else{
                            echo "<strong>" . $columnNames[$i] . "</strong>: <textarea name=\"" . $columnNames[$i] . "\" style=\"max-width: 100%;max-height: 100vh;\" rows=\"15\" cols=\"23\">" .  $row[$i] . "</textarea><br>";
                        }
                    }
                ?>
                <input class="btn btn-primary" type="submit">
                <button type="button" onclick="showDelete()" class="pull-left btn btn-danger">Delete</button>
                </form>
            </div>

            <form action="_php/deleteTodoProcess.php" id="DeleteForm">

                <?php 
                $_SESSION['Title'] = $row[0];
                ?>

                <p>Are you sure you want to delete Todo: <?php echo $row[0]?> ? </p>
                <button type="submit" onclick="deleteTodo(true)" class="pull-left btn btn-default">Delete</button>
                <button type="button" onclick="deleteTodo(false)" class="pull-left btn btn-default">Cancel</button>
            </form>

        </div>
    </body>
    <?php     
        mysqli_close($conn);
    ?>

</html>