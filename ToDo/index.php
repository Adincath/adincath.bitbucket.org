<!DOCTYPE html>
<html>
<head>
  <title>Welcome | To Do Er</title>
  <!-- jQuery -->
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <!-- Bootstrap -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
  <!-- Font used -->
  <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

  <!-- main css -->
  <link rel="stylesheet" type="text/css" href="_css/main.css">

  <script>
    
    function onSignIn(googleUser) {
      var profile = googleUser.getBasicProfile();
      console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
      console.log('Name: ' + profile.getName());
      console.log('Image URL: ' + profile.getImageUrl());
      console.log('Email: ' + profile.getEmail());
    }
  </script>

</head>

<body>
<?php include "nav.html" ?>

  <div class="col-sm-4" id="main-container">
    <h1>To-Do-Er!</h1>
    <hr>
    <p>The To Do projects will allow someone to create a simple persistent "To Do" list. More importantly for me, this provides a basic CRUD platform for me to expand upon as I continue to work on the project.</p>

    <p>I originally created this project to keep all of my other projects on track. I hope to use this web app in future projected to mitigate problems with work flow and deadlines </p>
    <a href="newTodo.php" class="col-sm-6 home-thumbnail">
      <span class="linkThumbnails glyphicon glyphicon-list-alt"></span>
    </a>
    <a href="" class="col-sm-6 home-thumbnail">
      <span class="linkThumbnails glyphicon glyphicon-list-alt"></span>
    </a>
  </div>
  <!-- Google Platform Library -->
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <meta name="google-signin-client_id" content="812054113962-j7ev61u562k51v34k6cvvdg92ftl1btp.apps.googleusercontent.com">
</body>
</html>